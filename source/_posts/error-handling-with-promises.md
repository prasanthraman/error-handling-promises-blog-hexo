---
title: Promises - Error Handling
---

# Error Handling with promises in JavaScript

Error handling is an important aspect of programming. It becomes very crucial when dealing with `promises` because `promises` are objects that represent the result of an asynchronous opertaion. If errors aren't handled properly it may lead to unexpected behavious in code, errors and crashes.

### Different ways to handle errors in promises:
* Using the `catch` method:

The `catch` method allows us to handle errors in a promise chain. Even if a single `then` block fails, the `catch` method gets called and error and be displayed or handled inside that method.

Here is an code sample:
```
Promise
    .then(() => {
        //Success
    })
    .catch((error) => {
        //Error can be handled here
    })
```

* Using error function as second argument in `then` method:

The `then` methods takes two functions as arguements. First function is to handle success or fullfiled and the second function is meant to handle errors

Here is an code sample:
```
Promise
    .then(() => { }, (error) => {
        //Handle error here
    })
```


* Using a `try-catch` block:

The `try-catch` block is useful to perform some actions/handle errors even before the promise is fullfiled or rejected.

Here is an code sample:
```
try{
    //Perform some task here

}catch(err){
    //Handle error if code in try block fails or throws an error
}
```


### Why is error handling so important especially with promises?

* Asynchronous code is generally hard to debug. It is challenging to understand what step is currently being executed and errors doesn't get thrown immediately. And hence proper error handling should be implemented to catch specific errors and fix them.

* Errors provide valuable information about what went wrong, where does the error occur and why its an error. By using this information we can fix our bugs and improve our code.


